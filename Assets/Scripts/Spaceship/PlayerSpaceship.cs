using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        public event Action TakeDamage;
        [SerializeField] private Bullet.TentacleBomb ult;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(audioSource != null,"audioSource cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, transform.rotation);
            bullet.Init();
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.Fire);
        }

        public void Ulti()
        {
            var ulti = Instantiate(ult,gunPosition.position,transform.rotation);
            ulti.Init();
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                TakeDamage?.Invoke();
                StartCoroutine(TakingHit());
                return;
            }
            TakeDamage?.Invoke();
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}