using System.Collections;
using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet.Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected AudioSource audioSource;

        [SerializeField] private bool takingDamage = false;
        protected internal int Hp { get; set; }
        public float Speed { get; private set; }
        private Bullet.Bullet bullet;

        protected void Init(int hp, float speed, Bullet.Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            this.bullet = bullet;
        }

        private void Update()
        {
            if (takingDamage)
            {
                float randomPosX = Random.insideUnitCircle.x * 0.02f;
                float randomPosY = Random.insideUnitCircle.y * 0.02f;
                transform.position += new Vector3(randomPosX,randomPosY);
            }
        }

        internal IEnumerator TakingHit()
        {
            Vector2 originalPos = transform.position;
            if (!takingDamage)
            {
                takingDamage = !takingDamage;
            }
            yield return new WaitForSeconds(0.30f);
            takingDamage = false;
            //transform.position = originalPos;
        }

        public abstract void Fire();
    }
}