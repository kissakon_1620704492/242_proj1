using System;
using Enemy;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        public void Init(int hp, float speed,Transform target)
        {
            base.Init(hp, speed, defaultBullet);
            GetComponent<EnemyController>().PlayerTran = target;
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                StartCoroutine(TakingHit());
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            //Destroy(gameObject.GetComponent<EnemyController>());
            Destroy(gameObject,1);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet,gunPosition.position,transform.rotation);
            bullet.Init();
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.EnemyFire);
        }
    }
}