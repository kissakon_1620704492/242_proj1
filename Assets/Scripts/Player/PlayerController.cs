﻿using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private int turnSpeed;
        
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private Vector2 delta;

        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private Vector3 mousePosition;
        private Camera mainCamera;
        private void Awake()
        {
            mainCamera = Camera.main;
            InitInput();
            CreateMovementBoundary();
        }
        
        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.Ulti.performed += OnUlti;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.Fire();
        }

        private void OnUlti(InputAction.CallbackContext obj)
        {
            playerSpaceship.Ulti();
        }

        private void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }
            if (obj.canceled)
            {
                movementInput = Vector2.zero; 
            }
        }
        
        private void Update()
        {
            Move();
            PointerUpdate();
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceship.Speed;

            var newPosition = transform.position;
            newPosition.x += inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y += inputVelocity.y * Time.smoothDeltaTime;
            //newPosition.x = transform.position.x + inputVelocity.x * Time.smoothDeltaTime;
            //newPosition.y = transform.position.y + inputVelocity.y * Time.smoothDeltaTime;

            delta = mousePosition - newPosition;

            transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(Vector3.forward,delta), Time.fixedDeltaTime * turnSpeed );

            // Clamp movement within boundary
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;
        }
        
        private void CreateMovementBoundary()
        {
            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
            
            var offset = spriteRenderer.bounds.size;
            minX = FieldManager.Instance.MinX() + offset.x / 2;
            maxX = FieldManager.Instance.MaxX() - offset.x / 2;
            minY = FieldManager.Instance.MinY() + offset.y / 2;
            maxY = FieldManager.Instance.MaxY() - offset.y / 2;
            //minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            //maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            //minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            //maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void PointerUpdate()
        {
            Debug.Assert(mainCamera != null, "Main camera cannot be null");
            
            mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        
        

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
