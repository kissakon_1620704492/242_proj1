﻿using System.Collections;
using Spaceship;
using UnityEngine;

namespace Bullet
{
    public class TentacleBomb : BaseBullet
    {
        private Tentacle[] tentacles;
        public new void Init()
        {
            StartCoroutine(Starting());
        }

        private IEnumerator Starting()
        {
            spark.Stop();
            tentacles = GetComponentsInChildren<Tentacle>();
            foreach (Tentacle tentacle in tentacles)
            {
                tentacle.Init();
            }
            yield return new WaitForSeconds(0.6f);
            base.Init();

            Debug.Assert(tentacles != null && tentacles.Length > 0,"tentacles cannot be null");
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            IDamagable target = other.gameObject.GetComponent<IDamagable>();
            if (target != null)
            {
                target.TakeHit(damage);
            }
        }
    }
}
