﻿using Manager;
using Spaceship;
using UnityEngine;

namespace Bullet
{
    public class BaseBullet : MonoBehaviour
    {
        [SerializeField] private new Rigidbody2D rigidbody2D;
        [SerializeField] internal int damage;
        [SerializeField] internal float speed;
        [SerializeField] protected ParticleSystem spark;
        
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;

        private Camera mainCamera;
        
        protected void Init()
        {
            Move();
            spark.Stop();
        }

        private void Awake()
        {
            mainCamera = Camera.main;
            
            Debug.Assert(mainCamera != null,"mainCamera cannot be null");
            Debug.Assert(rigidbody2D != null, "rigidbody2D cannot be null");
            
            CreateBorder();
        }

        private void FixedUpdate()
        {
            if (BorderChecker())
            {
                Destroy(gameObject);
            }
        }

        private void Move()
        {
            rigidbody2D.velocity = transform.up * speed;
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            IDamagable target = other.gameObject.GetComponent<IDamagable>();
            if (target != null)
            {
                var sparkLocation = Instantiate(spark, transform.position, Quaternion.LookRotation(-transform.up));
                Destroy(sparkLocation.gameObject,1);
                target.TakeHit(damage);
                Destroy(gameObject);
            }
        }

        private void CreateBorder()
        {
            var spriteRenderer = rigidbody2D.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
            var offset = spriteRenderer.bounds.size;
            minX = FieldManager.Instance.MinX() - offset.x;
            maxX = FieldManager.Instance.MaxX() + offset.x;
            minY = FieldManager.Instance.MinY() - offset.y;
            maxY = FieldManager.Instance.MaxY() + offset.y;
        }

        private bool BorderChecker()
        {
            Vector3 position = rigidbody2D.position;
            return position.y > maxY || position.y < minY || position.x > maxX || position.x < minX;
        }

    }
}
