﻿using System;
using UnityEngine;
using Random = System.Random;

namespace Bullet
{
    public class Tentacle : MonoBehaviour
    {
        [SerializeField] private int length;
        [SerializeField] private LineRenderer tentacle;
        [SerializeField] private Vector3[] segment;
        private Vector3[] segmentV;

        [SerializeField] private Transform mainPos;
        [SerializeField] private float dist;
        [SerializeField] private float smooth;
        [SerializeField] private float tentacleSpeed;
        [SerializeField] private float wiggleSpeed;
        [SerializeField] private float wiggleMag;
        [SerializeField] private Transform wiggleDir;

        public void Init()
        {
            var rnd = new Random();
            float mag = rnd.Next(-60, 60);
            wiggleMag = mag;
            for (var i = 1; i < segment.Length; i++)
            {
                segment[i] = mainPos.position;
            }
        }
        private void Awake()
        {
            tentacle.positionCount = length;
            segment = new Vector3[length];
            segmentV = new Vector3[length];
            
            Debug.Assert(tentacle != null,"tentacle cannot be null");
            Debug.Assert(segment != null && segment.Length > 0, "segment array need to have something in it and cant be null");
            Debug.Assert(mainPos != null,"mainPos cannot be null");
            Debug.Assert(wiggleDir != null,"wiggleDir cannot be null");
        }

        private void Update()
        {
            wiggleDir.localRotation = Quaternion.Euler(0,0,Mathf.Sin(Time.time * wiggleSpeed) * wiggleMag);
            segment[0] = mainPos.position;
            for (int i = 1; i < segment.Length; i++)
            {
                segment[i] = Vector3.SmoothDamp(segment[i],segment[i-1] + mainPos.right * dist,ref segmentV[i],smooth + i / tentacleSpeed);
            }
            tentacle.SetPositions(segment);
        }
    }
}
