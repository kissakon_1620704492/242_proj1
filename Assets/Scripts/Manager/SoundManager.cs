﻿using System;
using Singleton;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioSource deathHandler;

        //public static SoundManager Instance;

        [Serializable]
        private struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
            [Range(0.0f, 1.0f)] public float AudioVolume;
        }

        internal enum Sound
        {
            BGM,
            Fire,
            EnemyFire,
            Death,
        }
    
        internal void Play(AudioSource audioSource, Sound sound)
        {
            Debug.Assert(audioSource != null,$"audioSource != null {sound}");

            audioSource.clip = GetAudioClip(sound);
            audioSource.volume = GetAudioVolume(sound);
            audioSource.Play();
        }

        internal void PlayBGM()
        {
            Play(audioSource,Sound.BGM);
            audioSource.loop = true;
        }

        internal void PlayDeath()
        {
            Play(deathHandler,Sound.Death);
        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }
            Debug.Assert(false,$"Cannot Find {sound}");
            return null;
        }

        private float GetAudioVolume(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioVolume;
                }
            }
            
            Debug.Assert(false,$"Cannot Find {sound}");
            return 0;
        }

        private void Awake()
        {
            Debug.Assert(audioSource != null,"audioSource != null");
            Debug.Assert(soundClips != null,"soundClips != null");
            //if (Instance == null)
            //{
            //    Instance = this;
            //}
            //
            //DontDestroyOnLoad(this);
        }
    }
}
