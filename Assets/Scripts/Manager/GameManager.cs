﻿using System;
using System.Collections;
using Singleton;
using Spaceship;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public event Action OnRestarted;
        [SerializeField] private Button startButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button[] quitButtons;
        //[SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        //[SerializeField] private ScoreManager scoreManager;
        [SerializeField] private int enemyCount;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private ParticleSystem explodeEffect;
        
        private PlayerSpaceship spaceShip;
        private EnemySpaceship enemyShip;
        private int OriginalHealth => playerSpaceshipHp;

        private int enemyCounter = 0;
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(nextButton != null, "nextButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(quitButtons != null, "quitButton cannot be null");
            //Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            //Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemyCount > 0,"enemyCount has to be more than 0");
            Debug.Assert(explodeEffect != null,"explodeEffect cannot be null");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnReStartButtonClicked);
            nextButton.onClick.AddListener(OnNextButtonClicked);
            foreach (var quitButton in quitButtons)
            {
                quitButton.onClick.AddListener(OnQuitClicked);
            }
            
            UIManager.Instance.SetActive(UIManager.DialogName.startDialog,true);
        }

        private void OnStartButtonClicked()
        {
            UIManager.Instance.SetActive(UIManager.DialogName.startDialog,false);
            UIManager.Instance.SetActive(UIManager.DialogName.scoreText,false);
            StartGame();
        }
        private void OnReStartButtonClicked()
        {
            UIManager.Instance.SetActive(UIManager.DialogName.restartDialog	,false);
            UIManager.Instance.SetActive(UIManager.DialogName.scoreText,false);
            playerSpaceshipHp = OriginalHealth;
            StartGame();
        }

        private void OnNextButtonClicked()
        {
            StartCoroutine(LoadScene());
        }

        private void OnQuitClicked()
        {
            if (UnityEditor.EditorApplication.isPlaying)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
            else
            {
                Application.Quit();
            }
        }

        private IEnumerator LoadScene()
        {
            yield return SceneManager.LoadSceneAsync("Game 2");
            UIManager.Instance.SetActive(UIManager.DialogName.nextSceneDialog,false);
            playerSpaceshipHp = HealthManager.Instance.GetHealth();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SoundManager.Instance.PlayBGM();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spaceShip = Instantiate(playerSpaceship);
            spaceShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            HealthManager.Instance.Init(playerSpaceshipHp);
            spaceShip.TakeDamage += UpdateHealthOnPlayerTakeDamage;
            spaceShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void UpdateHealthOnPlayerTakeDamage()
        {
            spaceShip.TakeDamage -= UpdateHealthOnPlayerTakeDamage;
            HealthManager.Instance.UpdateHealth(spaceShip.Hp);
            spaceShip.TakeDamage += UpdateHealthOnPlayerTakeDamage;
        }

        private void OnPlayerSpaceshipExploded()
        {
            MakeExplode(spaceShip.transform);
            SoundManager.Instance.PlayDeath();
            enemyCounter = 0;
            var enemySp = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in enemySp)
            {
                Destroy(enemy);
            }
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            Vector3 deltaPos = spaceShip.transform.position - enemySpaceship.transform.position;
            enemyShip = Instantiate(enemySpaceship,enemyShip.transform.position,quaternion.LookRotation(Vector3.forward,deltaPos));
            enemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed,spaceShip.transform);
            enemyShip.OnExploded += OnEnemySpaceshipExploded;
            enemyCounter++;
        }

        private void OnEnemySpaceshipExploded()
        {
            MakeExplode(enemyShip.transform);
            SoundManager.Instance.PlayDeath();
            ScoreManager.Instance.AddScore();
            if (enemyCounter <= enemyCount)
            {
                SpawnEnemySpaceship();
            }
            else
            {
                StartCoroutine(FinishLevel());
            }
        }

        private void MakeExplode(Transform target)
        {
            ParticleSystem explode = Instantiate(explodeEffect,target.position,quaternion.identity);
            Destroy(explode.gameObject,1);
        }

        private IEnumerator FinishLevel()
        {
            yield return new WaitForSeconds(0.5f);
            spaceShip.gameObject.SetActive(false);
            enemyCount = FieldManager.Instance.GetMonsterCount();
            enemyCounter = 0;
            ScoreManager.Instance.SetTotalScore();
            UIManager.Instance.SetActive(UIManager.DialogName.nextSceneDialog,true);
        }

        private void Restart()
        {
            UIManager.Instance.SetActive(UIManager.DialogName.restartDialog	,true);
            UIManager.Instance.SetActive(UIManager.DialogName.scoreText,true);
            OnRestarted?.Invoke();
        }
    }
}
