﻿using System;
using Singleton;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager
{
    public class HealthManager : MonoSingleton<HealthManager>
    {
        [SerializeField] private float health;
        [SerializeField] private int numberOfHearts;
        [SerializeField] private Image[] hearts;
        [SerializeField] private Sprite fullHeart;
        [SerializeField] private Sprite halfHeart;

        private void Awake()
        {
            Debug.Assert(fullHeart != null,"fullHeart sprite cannot be null");
            Debug.Assert(halfHeart != null,"halfHeart sprite cannot be null");
            Debug.Assert(hearts != null && hearts.Length > 0,"hearts array cannot be null");
        }

        internal void Init(float hp)
        {
            health = hp;
            numberOfHearts = (int) ((health / 20) + 0.5f);
        }

        internal void UpdateHealth(int spaceshipHp)
        {
            health = spaceshipHp;
        }

        internal int GetHealth()
        {
            return (int) health;
        }
        private void Update()
        {
            for (var i = 0; i < hearts.Length; i++)
            {
                //hearts[i].sprite = i < health * 0.1 ? fullHeart : halfHeart;
                //if (health * 0.01 > numberOfHearts)
                //{
                //    numberOfHearts = (int) (health * 0.01);
                //}
                if (health/20 < numberOfHearts)
                {
                    numberOfHearts = (int) ((health / 20) + 0.5f);
                }
                hearts[i].sprite = (i+1) * 20 <= health ? fullHeart : halfHeart;
                hearts[i].enabled = i < numberOfHearts;
            }
        }
    }
}
