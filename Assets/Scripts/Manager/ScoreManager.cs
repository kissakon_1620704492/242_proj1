﻿using Singleton;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        //public static ScoreManager Instance;
        //[SerializeField] private TextMeshProUGUI scoreText;
        //private GameManager gameManager;
        private int saveScore = 0;
        
        internal void Init()// GameManager gameManager
        {
            //this.gameManager = gameManager;
            //this.gameManager.OnRestarted += OnRestarted;
            GameManager.Instance.OnRestarted += OnRestarted;
            UIManager.Instance.SetActive(UIManager.DialogName.scoreText,true);
            ResetScore();
        }

        internal void AddScore()
        {
            saveScore++;
            UIManager.Instance.SetScore(saveScore);
        }
        
        internal void AddScore(int score)
        {
            saveScore += score;
            UIManager.Instance.SetScore(saveScore);
        }

        internal void UpdateScore(int score)
        {
            saveScore += score;
            UIManager.Instance.SetScore(saveScore);
        }

        private void ResetScore()
        {
            saveScore = 0;
            UIManager.Instance.SetScore(saveScore);
        }

        internal void SetScore()
        {
            UIManager.Instance.SetScore(saveScore);
        }

        internal void SetTotalScore()
        {
            UIManager.Instance.SetTotalScore(saveScore);
        }
        
        /*private void Awake()
        {
            //Debug.Assert(scoreText != null, "scoreText cannot null");
            //if (Instance == null)
            //{
            //    Instance = this;
            //}
            //DontDestroyOnLoad(this);
        }*/
        
        private void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
            //UIManager.Instance.HideScore(true);
            UIManager.Instance.SetActive(UIManager.DialogName.scoreText,false);
            ResetScore();
        }
    }
}


