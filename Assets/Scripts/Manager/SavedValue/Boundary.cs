﻿using UnityEngine;

namespace Manager.SavedValue
{
    [CreateAssetMenu(fileName = "New Boundary",menuName = "Make/Boundary")]
    public class Boundary : ScriptableObject
    {
        public float maxX;
        public float minX;
        public float maxY;
        public float minY;

        public int monsterCount;
    }
}
