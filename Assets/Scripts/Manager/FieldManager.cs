﻿using Manager.SavedValue;
using Singleton;
using UnityEngine;

namespace Manager
{
    public class FieldManager : MonoSingleton<FieldManager>
    {
        [SerializeField] private Boundary boundary;
        private Camera mainCamera;

        private void Awake()
        {
            mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"mainCamera Cannot be null");
            boundary.minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x;
            boundary.maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x;
            boundary.minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y;
            boundary.maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y;
        }

        public float MinX()
        {
            return boundary.minX;
        }

        public float MaxX()
        {
            return boundary.maxX;
        }

        public float MinY()
        {
            return boundary.minY;
        }

        public float MaxY()
        {
            return boundary.maxY;
        }

        public int GetMonsterCount()
        {
            return boundary.monsterCount;
        }
    }
}
