using System;
using Singleton;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager : MonoSingleton<UIManager>
    {
        private TextMeshProUGUI scoreText;
        private TextMeshProUGUI totalScoreText;
        
        [SerializeField] private DialogBox[] dialogLists;

        [Serializable]
        private struct DialogBox
        {
            public DialogName Name;
            public GameObject Dialog;
        }

        public enum DialogName
        {
            startDialog,
            restartDialog,
            nextSceneDialog,
            scoreText,
            totalScoreText,
        }

        public void SetActive(DialogName name,bool trigger)
        {
            foreach (var thing in dialogLists)
            {
                if (thing.Name == name)
                {
                    thing.Dialog.SetActive(trigger);
                    return;
                }
            }
            Debug.Assert(false,$"Cannot Find {name}");
        }
        
        internal void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
        }

        internal void SetTotalScore(int score)
        {
            totalScoreText.text = $"{score}";
        }

        //public void HideScore(bool hide)
        //{
        //    scoreText.gameObject.SetActive(!hide);
        //}
        
        private void Awake()
        {
            foreach (var thing in dialogLists)
            {
                if (thing.Name == DialogName.totalScoreText)
                {
                    totalScoreText = thing.Dialog.GetComponent<TextMeshProUGUI>();
                }
            }
            foreach (var thing in dialogLists)
            {
                if (thing.Name == DialogName.scoreText)
                {
                    scoreText = thing.Dialog.GetComponent<TextMeshProUGUI>();
                }
            }
            Debug.Assert(scoreText != null, "scoreText cannot null");
            Debug.Assert(totalScoreText != null, "totalScoreText cannot null");

            SetCanvas();
        }

        private void SetCanvas()
        {
            var temp = GetComponent<Canvas>();
            temp.renderMode = RenderMode.ScreenSpaceCamera;
            temp.worldCamera = Camera.main;
        }
    }
}
