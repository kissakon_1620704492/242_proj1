﻿using Spaceship;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        internal Transform PlayerTran;
        [SerializeField] private EnemySpaceship enemyShip;
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private float turnSpeed;
        [SerializeField] private float fireRate;
        private float count = 0;
        private float originalTurnSpeed;

        private void Awake()
        {
            originalTurnSpeed = turnSpeed;
        }

        private void Update()
        {
            MoveToPlayer();
            MovementController();
            Firing();
        }

        private void Firing()
        {
            count += Time.deltaTime;
            if (count > fireRate)
            {
                enemyShip.Fire();
                count = 0;
            }
        }

        private void MovementController()
        {
            var delta = PlayerTran.position - transform.position;
            turnSpeed = delta.magnitude < 3.2f ? turnSpeed + Time.deltaTime : turnSpeed = originalTurnSpeed;

            transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(Vector3.forward,delta), Time.fixedDeltaTime * turnSpeed );
        }

        private void MoveToPlayer()
        {
            var delta = PlayerTran.position - transform.position;
            if (delta.magnitude > chasingThresholdDistance)
            {
                transform.position += delta.normalized * (enemyShip.Speed * Time.deltaTime);
            }
        }
    }   
}

